import delay, { sleep } from "./mockDelay";
import AppStorage from "../tools/AppStorage";
import { SubmissionError } from "redux-form";
//import * as codes from './errorCode';

// This file mocks a web API by working with the hard-coded data below.
// It uses sleep to simulate the delay of an AJAX call.
// All calls return promises
const exampleUser = {
  givenName: "Johann",
  familyName: "Doe" + Math.round(Math.random() * 100).toString(),
  name: "John",
  jobTitle: "Administrator",
  kind: "gravatar",
  noAvatar: true,
  gravatar: false
};
const getProfileSuccess = {
  user: exampleUser,
  success: true
};
const getProfileError = {
  success: false,
  error: {
    code: "profile-loading-server-error",
    message: "Profile unavailable"
  }
};
const successfulProfileResponse = {
  success: {
    code: "profile-updated",
    message: "Your profile has been successfully updated"
  },
  user: exampleUser
};
const successfulGravatarResponse = {
  success: {
    code: "gravatar-updated",
    message: "Your gravatar has been successfully updated"
  },
  user: {
    kind: "gravatar"
  }
};
const erroredResponse = {
  error: {
    code: "profile-server-error",
    message: "Server unavailable. Your profile has not been updated."
  }
};

class ProfileApi {
  static callGetProfileApi(callback) {
    return new Promise(() => {
      setTimeout(() => {
        if ("foo42bar" === AppStorage.getItem("accessToken")) {
          return callback(getProfileSuccess);
        } else {
          return callback(getProfileError);
        }
      }, delay);
    });
  }

  static isUsernameUnique(data) {
    return sleep(delay) // simulate server latency
      .then(() => {
        if (["John", "paul", "george", "ringo"].includes(data.name)) {
          const smallError = { name: "username is already taken" };
          throw smallError;
          //The code below throw a warning during build
          // throw {name: "username is already taken"};
          //The codes below do not work
          //throw new SubmissionError({ name: "username is already taken" });
          //throw new Error({ name: "username is already taken" });
        }
      });
  }

  static callUpdateApi(data, callback) {
    return sleep(delay).then(() => {
      const { kind, jobTitle } = data;
      if ("42" === jobTitle) {
        return callback(successfulProfileResponse);
      } else if ("file" === kind) {
        return callback(successfulGravatarResponse);
      } else {
        callback(erroredResponse);
        throw new SubmissionError(erroredResponse);
      }
    });
  }
}

export default ProfileApi;
