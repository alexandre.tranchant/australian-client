import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Route } from "react-router-dom";
import LoginPage from "./page/LoginPage";

export const AuthenticatedRoute = ({ component: ComposedComponent, ...rest }) => {
  class Authentication extends PureComponent {
    /* LoginPage displayed if not authenticated; otherwise, return the component imputted into <AuthenticatedRoute /> */
    handleRender = (props) => {
      if (!this.props.isAuthenticated) {
        return <LoginPage />;
      } else {
        return <ComposedComponent {...props} />;
      }
    };

    render() {
      return <Route {...rest} component={this.handleRender} />;
    }
  }

  const mapStateToProps = (state) => {
    return {
      isAuthenticated: state.authReducer.isAuthenticated
    };
  };

  const AuthenticationContainer = connect(mapStateToProps)(Authentication);
  return <AuthenticationContainer />;
};
