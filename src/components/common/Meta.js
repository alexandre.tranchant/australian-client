import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { withNamespaces } from "react-i18next";

const Meta = (props) => {
  const { code, description, keywords, t, title } = props;

  return (
    <Helmet>
      {title && <title>{t("meta.title." + code)}</title>}
      {description && <meta name="description" content={t("meta.description." + code)} />}
      {keywords && <meta name="keywords" content={t("meta.keywords." + code)} />}
    </Helmet>
  );
};

Meta.defaultProps = {
  description: true,
  keywords: true,
  title: true
};

Meta.propTypes = {
  code: PropTypes.string.isRequired,
  description: PropTypes.bool,
  keywords: PropTypes.bool,
  title: PropTypes.bool,
  t: PropTypes.func.isRequired
};

export default withNamespaces()(Meta);
