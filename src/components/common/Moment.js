import React from "react";
import PropTypes from "prop-types";
import { withNamespaces } from "react-i18next";
import Moment from "react-moment";
import "moment/locale/fr";
import "moment/locale/en-gb";

//FIXME rename into Moment
//TODO Add some explanations about this component.
const MyMoment = (props) => {
  const { i18n, ...other } = props;

  //removing translate props.
  delete other.tReady;
  delete other.i18nOptions;
  delete other.t;
  delete other.reportNS;
  delete other.defaultNS;

  return <Moment {...other} locale={i18n.language} />;
};

MyMoment.defaultProps = {
  withTitle: true
};

MyMoment.propTypes = {
  withTitle: PropTypes.bool,
  i18n: PropTypes.object.isRequired
};

export default withNamespaces()(MyMoment);
