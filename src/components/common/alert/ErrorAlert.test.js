import React from "react";
import ErrorAlert from "./ErrorAlert";
import { shallow, mount } from "enzyme";

describe("ErrorAlert", () => {
  it("without attributes should not have class foo", () => {
    const wrapper = shallow(<ErrorAlert>foo</ErrorAlert>);
    expect(wrapper.find("foo")).not.toExist();
  });
  it("without attributes should be centered, color should be danger, should be dismissible", () => {
    const wrapper = mount(<ErrorAlert>foo</ErrorAlert>);
    expect(wrapper.find(".text-center")).toExist();
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-danger")).toExist();
    expect(wrapper.find(".alert-dismissible")).toExist();
  });
  it("without attributes should contains children × from dismissible and foo ", () => {
    const wrapper = mount(<ErrorAlert>foo</ErrorAlert>);
    expect(wrapper.text()).toEqual("×foo");
    //expect(wrapper.html()).toEqual(<div class=\"text-center alert alert-danger alert-dismissible fade\" role=\"alert\"><button type=\"button\" class=\"close\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>foo</div>);
  });
  it("without controlled attribute should be centered, color should be danger, should NOT be dismissible", () => {
    const wrapper = mount(<ErrorAlert controlled>foo</ErrorAlert>);
    expect(wrapper.find(".text-center")).toExist();
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-danger")).toExist();
    expect(wrapper.find(".alert-dismissible")).not.toExist();
  });
  it("without attributes should contains children foo ", () => {
    const wrapper = mount(<ErrorAlert controlled>foo</ErrorAlert>);
    expect(wrapper.text()).toEqual("foo");
  });
});
