import React from "react";
import StatusAlert from "./StatusAlert";
import { mount } from "enzyme/build";

describe("StatusAlert test suite", () => {
  it("Returns an alert with an help message when all indicator are false", () => {
    const status = {
      error: {},
      isError: false,
      isLoaded: false,
      isLoading: false,
      isSuccess: false,
      isUnloadable: false,
      success: {}
    };

    const wrapper = mount(
      <StatusAlert code="bar" status={status}>
        foo
      </StatusAlert>
    );
    expect(wrapper.find(".text-center")).toExist();
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-info")).toExist();
    expect(wrapper.find(".alert-dismissible")).not.toExist();
    expect(wrapper.text()).toEqual("help.bar");
  });

  it("Returns an alert with an error message when isError is true", () => {
    const status = {
      error: {
        code: "foo",
        message: "barFoo"
      },
      isError: true,
      isLoaded: false,
      isLoading: false,
      isSuccess: false,
      isUnloadable: false,
      success: {}
    };

    const wrapper = mount(
      <StatusAlert code="bar" status={status}>
        foo
      </StatusAlert>
    );
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-danger")).toExist();
    expect(wrapper.find(".alert-success")).not.toExist();
    expect(wrapper.text()).toEqual("barFoo");
  });

  it("Returns an alert with an error message when isUnloadable is true", () => {
    const status = {
      error: {
        code: "foo",
        message: "barFoo"
      },
      isError: false,
      isLoaded: false,
      isLoading: false,
      isSuccess: false,
      isUnloadable: true,
      success: {}
    };

    const wrapper = mount(
      <StatusAlert code="bar" status={status}>
        foo
      </StatusAlert>
    );
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-danger")).toExist();
    expect(wrapper.find(".alert-success")).not.toExist();
    expect(wrapper.text()).toEqual("barFoo");
  });

  it("Returns an alert with a success message when isSuccess is true", () => {
    const status = {
      error: {},
      isError: false,
      isLoaded: false,
      isLoading: false,
      isSuccess: true,
      isUnloadable: false,
      success: {
        code: "foo",
        message: "barFoo"
      }
    };

    const wrapper = mount(
      <StatusAlert code="bar" status={status}>
        foo
      </StatusAlert>
    );
    expect(wrapper.find(".alert")).toExist();
    expect(wrapper.find(".alert-danger")).not.toExist();
    expect(wrapper.find(".alert-success")).toExist();
    expect(wrapper.text()).toEqual("barFoo");
  });
});
