import React from "react";
import Submit from "./Submit";
import { mount } from "enzyme";

describe("Submit", () => {
  it("when no properties provided should render correctly", () => {
    const fake = () => {
      return null;
    };

    const wrapper = mount(<Submit isPending={false} name={"foo"} onClick={fake} />);
    expect(wrapper.text()).toEqual("form.foo.submit");
    expect(wrapper.find("button")).toExist();

    //TODO complete test
  });
});
