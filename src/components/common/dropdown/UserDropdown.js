import React from "react";
import PropTypes from "prop-types";
import Gravatar from "../Gravatar";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { faIdCardAlt, faSignOutAlt, faUser, faUserCog } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { logout } from "../../../actions/authActions";
import { withNamespaces } from "react-i18next";

library.add(faIdCardAlt, faSignOutAlt, faUser, faUserCog);

const UserDropdown = (props) => {
  const { email, gravatar, logout, t, username } = props;

  return (
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret>
        {gravatar && <Gravatar email={email} size={20} className="mr-1" />}
        {!gravatar && <FontAwesomeIcon icon="user" fixedWidth className="mr-1" />}
        {username}
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem to="/profile/general" tag={NavLink}>
          <FontAwesomeIcon fixedWidth icon="id-card-alt" /> {t("navbar.user-profile")}
        </DropdownItem>
        <DropdownItem>
          <FontAwesomeIcon fixedWidth icon="user-cog" /> {t("navbar.user-settings")}
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem onClick={logout}>
          <FontAwesomeIcon fixedWidth icon="sign-out-alt" /> {t("navbar.user-logout")}
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

//FIXME Why this code?
UserDropdown.contextTypes = {
  router: PropTypes.object
};

UserDropdown.propTypes = {
  email: PropTypes.string.isRequired,
  gravatar: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired
};

// Redux connect begin here
function mapStateToProps(state) {
  return {
    email: state.authReducer.email,
    gravatar: state.authReducer.gravatar,
    username: state.authReducer.username
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout())
  };
}

export default withNamespaces()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserDropdown)
);
