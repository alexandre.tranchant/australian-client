import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactCrop from "react-image-crop";
import { Col, FormGroup, Input, Label } from "reactstrap";
import { withNamespaces } from "react-i18next";
import { fieldInputPropTypes, fieldMetaPropTypes } from "redux-form";

class AvatarInput extends PureComponent {
  state = {
    src: null,
    crop: {
      x: 10,
      y: 10,
      aspect: 1,
      height: 80
    }
  };

  onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => this.setState({ src: reader.result }));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
  };

  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, "newFile.jpeg");
    this.setState({ croppedImageUrl });
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      0,
      0,
      pixelCrop.width,
      pixelCrop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob((file) => {
        file.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(file);
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }

  render() {
    const { crop, croppedImageUrl, src } = this.state;
    const { input, meta, t } = this.props;

    return (
      <div>
        <FormGroup row >
          <Col sm={4}>
            <Label for={"photo"}>{t("form." + meta.form + "." + input.name + ".label")}</Label>
          </Col>
          <Col sm={{ size: 8 }}>
            <Input type="file" name="photo" onChange={this.onSelectFile} />
          </Col>
        </FormGroup>
        <FormGroup row className="mb-3">
          <Col sm={4}>
            {croppedImageUrl && (
              <img alt={t("common.cropped-image")} src={croppedImageUrl} className="img-fluid img-thumbnail" />
            )}
          </Col>
          <Col sm={8}>
            {src && (
              <ReactCrop
                className="img-fluid img-thumbnail"
                src={src}
                alt={t("common.original-image")}
                crop={crop}
                onImageLoaded={this.onImageLoaded}
                onComplete={this.onCropComplete}
                onChange={this.onCropChange}
              />
            )}
          </Col>
        </FormGroup>
      </div>
    );
  }
}

AvatarInput.propTypes = {
  input: PropTypes.shape(fieldInputPropTypes).isRequired, //redux-form
  meta: PropTypes.shape(fieldMetaPropTypes).isRequired, //redux-form
  t: PropTypes.func.isRequired
};

export default withNamespaces()(AvatarInput);
