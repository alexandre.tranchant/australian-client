import React from "react";
import PropTypes from "prop-types";
import { Input } from "reactstrap";
import { withNamespaces } from "react-i18next";

const InputLoading = (props) => {
  const { isLoading, t, ...other } = props;
  delete other["tReady"]; //because of translation.

  if (isLoading) {
    return <Input {...other} value={t("message.loading")} disabled />;
  }

  return <Input {...other} />;
};

InputLoading.defaultProps = {
  isLoading: true
};

// The propTypes.
InputLoading.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired
};

export default withNamespaces()(InputLoading);
