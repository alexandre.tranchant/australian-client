import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Trans, withNamespaces } from "react-i18next";
import { faExclamationTriangle, faSync } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { Button } from "reactstrap";

library.add(faExclamationTriangle, faSync);

const Error = (props) => {
  const { code, message, onClick, t } = props;

  return (
    <div className="text-danger">
      <FontAwesomeIcon icon="exclamation-triangle" className="mr-1" />
      <Trans i18nKey={code}>{t(message)}</Trans>
      {typeof onClick === "function" && (
        <Button onClick={onClick} color="danger" className="mt-2">
          <FontAwesomeIcon icon="sync" className="mr-1" />
          {t("button.retry")}
        </Button>
      )}
    </div>
  );
};

Error.propTypes = {
  code: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  t: PropTypes.func.isRequired
};

export default withNamespaces()(Error);
