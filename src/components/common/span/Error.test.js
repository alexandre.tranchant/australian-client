import React from "react";
import Error from "./Error";
import { mount } from "enzyme";

describe("Error div", () => {
  it("when no function provided should not render button", () => {
    const wrapper = mount(<Error code={"foo"} message={"bar"} />);
    expect(wrapper.find("div.text-danger")).toExist();
    expect(wrapper.find("button")).not.toExist();
  });
  it("when a function is provided should render button", () => {
    const fake = () => {};
    const wrapper = mount(<Error code={"foo"} message={"bar"} onClick={fake} />);
    expect(wrapper.find("div.text-danger")).toExist();
    expect(wrapper.find("button")).toExist();
  });
});
