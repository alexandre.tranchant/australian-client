import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { withNamespaces } from "react-i18next";

library.add(faSpinner);

const Loading = (props) => {
  const { color, t } = props;
  const className = "text-" + color;

  return (
    <div className={className}>
      <FontAwesomeIcon icon="spinner" spin className="mr-1" />
      {t("error.loading")}
    </div>
  );
};

Loading.defaultProps = {
  color: "secondary"
};

Loading.propTypes = {
  color: PropTypes.oneOf(["danger", "info", "muted", "primary", "secondary", "success", "warning", "white"]),
  t: PropTypes.func.isRequired
};

export default withNamespaces()(Loading);
