import React from "react";
import Loading from "./Loading";
import { mount } from "enzyme";

describe("Loading div", () => {
  it("when no color provided should render a text-secondary", () => {

    const wrapper = mount(<Loading />);
    expect(wrapper.find("div.text-secondary")).toExist();

  });
  it("when a valid color is provided should not render text-secondary but this text-color", () => {

    const wrapper = mount(<Loading color="success"/>);
    expect(wrapper.find("div.text-secondary")).not.toExist();
    expect(wrapper.find("div.text-success")).toExist();

  });
});