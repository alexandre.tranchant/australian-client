import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import AvatarForm from "../../form/AvatarForm";
import StatusAlert from "../../common/alert/StatusAlert";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getProfile } from "../../../actions/profileActions";
import { withNamespaces } from "react-i18next";

class AvatarContainer extends PureComponent {
  constructor(props) {
    super(props);

    props.actions.getProfile();
  }

  render() {
    const { actions, status, t } = this.props;
    const { isLoading, isUnloadable } = status;

    return (
      <div>
        <h2>{t("title.avatar-general")}</h2>
        <StatusAlert code="avatar" status={status} onReload={actions.getProfile} />
        <AvatarForm isLoading={isLoading} isUnloadable={isUnloadable} />
      </div>
    );
  }
}

// The propTypes.
AvatarContainer.propTypes = {
  status: PropTypes.shape({
    error: PropTypes.object.isRequired,
    isError: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    isUnloadable: PropTypes.bool.isRequired,
    success: PropTypes.object.isRequired
  }).isRequired
};

// Redux connect begin here
function mapStateToProps(state) {
  return {
    status: {
      error: state.profileReducer.error,
      isError: state.profileReducer.isProfileError,
      isLoading: state.profileReducer.isProfileLoading,
      isSuccess: state.profileReducer.isProfileSuccess,
      isUnloadable: state.profileReducer.isProfileUnloadable,
      success: state.profileReducer.success
    }
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ getProfile }, dispatch)
  };
}

//connect is returning a function, that explains the )(
export default withNamespaces()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AvatarContainer)
);
