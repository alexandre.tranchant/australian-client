import React from "react";
import PropTypes from "prop-types";
import AvatarInput from "../common/input/AvatarInput";
import FormRadioBoxGroup from "../formgroup/abstract/FormRadioBoxGroup";
import Reset from "../common/button/Reset";
import Submit from "../common/button/Submit";
import { Col, Form, Row } from "reactstrap";
import { Field, reduxForm, propTypes } from "redux-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { updateProfile } from "../../actions/profileActions";

library.add(faUser);

export const validate = (values) => {
  console.dir(values);
  const errors = {};

  //TODO values.photo is a fileList. and isEmpty throw an error.
  // if (!values.photo || isEmpty(values.photo)) {
  //   errors.photo = "photo is required";
  // }
  //TODO verify kind field.

  return errors;
};

const AvatarForm = (props) => {
  const { actions, handleSubmit, isLoading, pristine, reset, submitting, isUnloadable } = props;

  const fieldProps = {
    disabled: submitting || isLoading,
    isLoading: submitting || isLoading,
    isUnloadable
  };

  return (
    <Form onSubmit={handleSubmit(actions.updateProfile)}>
      <fieldset className="form-group">
        <Row>
          <legend className="col-form-label col-sm-4 pt-0">TOTO</legend>
          <Col sm={8}>
            <Field component={FormRadioBoxGroup} {...fieldProps} name="kind" option="none" />
            <Field component={FormRadioBoxGroup} {...fieldProps} name="kind" option="gravatar" />
            <Field component={FormRadioBoxGroup} {...fieldProps} name="kind" option="file" />
          </Col>
        </Row>
      </fieldset>
      <Field component={AvatarInput} {...fieldProps} name="photo" />
      <div className="text-right">
        <Reset disabled={pristine || submitting} onClick={reset} />
        <Submit
          disabled={pristine}
          isPending={submitting}
          name="profile"
          onClick={handleSubmit(actions.updateProfile)}
        />
      </div>
    </Form>
  );
};

// The propTypes.
AvatarForm.propTypes = {
  actions: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  ...propTypes
};

// Redux connect begin here
function mapStateToProps(state) {
  return {
    initialValues: state.profileReducer.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ updateProfile }, dispatch)
  };
}

// Redux form begin here
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    enableReinitialize: true,
    form: "avatar",
    validate
  })(AvatarForm)
);
//Be careful, do not remove validators, because if it is not preloaded, form is destroy and rebuild.
