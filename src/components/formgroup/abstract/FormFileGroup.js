import React from "react";
import PropTypes from "prop-types";
import HelpBlock from "../../common/help/HelpBlock";
import HelpBlockErrors from "../../common/help/HelpBlockErrors";
import { Col, FormGroup, Input, Label } from "reactstrap";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { fieldInputPropTypes, fieldMetaPropTypes } from "redux-form";
import { library } from "@fortawesome/fontawesome-svg-core";
import { withNamespaces } from "react-i18next";

library.add(faCamera);

const FormAllGroup = (props) => {
  const {
    children,
    disabled,
    input,
    meta: { error, form, submitting, touched },
    t
  } = props;

  const label = t("form." + form + "." + input.name + ".label");
  const help = t("form." + form + "." + input.name + ".helpBlock");
  const className = error ? "is-invalid" : "";
  const copyInput = input;
  delete copyInput.value;

  return (
    <FormGroup row>
      <Label for={input.name} sm={4}>
        {label}
      </Label>
      <Col sm={8}>
        <Input {...copyInput} type="file" className={className} disabled={disabled || submitting} />
        {touched && error && <HelpBlockErrors errors={[error]} />}
        {help.length > 0 && ((touched && !!error) || <HelpBlock>{help}</HelpBlock>)}
        {children}
      </Col>
    </FormGroup>
  );
};

FormAllGroup.defaultProps = {
  disabled: false,
  helpBlock: false,
  icon: "align-justify",
  isUnloadable: false,
  type: "text"
};

// The propTypes.
FormAllGroup.propTypes = {
  children: PropTypes.any,
  disabled: PropTypes.bool,
  helpBlock: PropTypes.bool,
  icon: PropTypes.string, //The XXXFormGroup
  input: PropTypes.shape(fieldInputPropTypes).isRequired, //redux-form
  isUnloadable: PropTypes.bool,
  meta: PropTypes.shape(fieldMetaPropTypes).isRequired, //redux-form
  t: PropTypes.func.isRequired, //translate
  type: PropTypes.oneOf(["text", "password", "email", "confirmation"]) //The XXXFormGroup
};

export default withNamespaces(["translations", "validators"])(FormAllGroup);
