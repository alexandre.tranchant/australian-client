import React from "react";
import PropTypes from "prop-types";
import HelpBlock from "../../common/help/HelpBlock";
import HelpBlockErrors from "../../common/help/HelpBlockErrors";
import { Input, Label } from "reactstrap";
import { fieldInputPropTypes, fieldMetaPropTypes } from "redux-form";
import { withNamespaces } from "react-i18next";

const FormRadioBoxGroup = (props) => {
  const {
    children,
    disabled,
    input,
    meta: { error, form },
    option,
    required,
    t
  } = props;

  const label = t("form." + form + "." + input.name + "." + option + ".label");
  const className = disabled ? "text-muted" : "";
  const helpBlock = t("form." + form + "." + input.name + "." + option + ".helpBlock");
  const messages = ["form-" + form + "-" + input.name + "-unchecked"];

  return (
    <div className={"form-check"}>
      <Label check className={className}>
        <Input
          {...input}
          checked={input.value === option ? "checked" : ""}
          type="radio"
          value={option}
          disabled={disabled}
          required={required}
        />
        {label}
      </Label>
      {!required && helpBlock && <HelpBlock>{helpBlock}</HelpBlock>}
      {error && <HelpBlockErrors errors={messages} />}
      {children}
    </div>
  );
};

// The propTypes.
FormRadioBoxGroup.defaultProps = {
  disabled: false,
  formName: "general",
  required: false
};

FormRadioBoxGroup.propTypes = {
  children: PropTypes.any,
  disabled: PropTypes.bool,
  input: PropTypes.shape(fieldInputPropTypes).isRequired, //redux-form
  meta: PropTypes.shape(fieldMetaPropTypes).isRequired, //redux-form
  option: PropTypes.string.isRequired,
  required: PropTypes.bool,
  t: PropTypes.func.isRequired
};

export default withNamespaces()(FormRadioBoxGroup);
