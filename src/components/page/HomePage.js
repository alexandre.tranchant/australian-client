import React from "react";
import PropTypes from "prop-types";
import Header from "../common/Header";
import Meta from "../common/Meta";
import Moment from "../common/Moment";
import { withNamespaces, Trans } from "react-i18next";

const HomePage = ({ i18n, t }) => {
  const day = "2006-10-13";

  return (
    <div>
      <Meta code="homepage" />
      <Header />
      <div className="App-header">
        <h1>{t("title.app")}</h1>
      </div>
      <div className="App-intro">
        <Trans i18nKey="description.part1">
          To get started, edit <code>src/App.js</code> and save to reload.
        </Trans>
      </div>
      <div>{t("description.part2")}</div>
      <div>
        <Moment locale={i18n.language} fromNow>
          {day}
        </Moment>
      </div>
      <div>
        <Moment locale={i18n.language} format={"LLLL"}>
          {day}
        </Moment>
      </div>
    </div>
  );
};

// The propTypes.
HomePage.propTypes = {
  i18n: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired
};

export default withNamespaces()(HomePage);
