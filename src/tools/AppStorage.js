class AppStorage {
  static getItem(item, defaultValue = "") {
    let storedValue = null;
    if (localStorage.getItem(item)) {
      storedValue = localStorage.getItem(item);
    } else if (sessionStorage.getItem(item)) {
      storedValue = sessionStorage.getItem(item);
    }
    if (null === storedValue) {
      return defaultValue;
    }

    //@see https://stackoverflow.com/questions/3263161/cannot-set-boolean-values-in-localstorage
    if (typeof defaultValue === "boolean") {
      storedValue = "true" === storedValue;
    } else if (typeof defaultValue === "number") {
      storedValue = Number(storedValue);
      if (isNaN(storedValue)) {
        return defaultValue;
      }
    }
    return storedValue;
  }
}

export default AppStorage;
