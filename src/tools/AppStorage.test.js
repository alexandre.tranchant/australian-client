import React from "react";
import AppStorage from "./AppStorage";

describe("AppStorage test suite", () => {
  it("return default values when nothing is stored", () => {
    expect(AppStorage.getItem("foo", "toto")).toEqual("toto");
    expect(AppStorage.getItem("foo", false)).toEqual(false);
    expect(AppStorage.getItem("foo", true)).toEqual(true);
    expect(AppStorage.getItem("foo", 0)).toEqual(0);
    expect(AppStorage.getItem("foo", 13)).toEqual(13);
  });
  it("return empty values when nothing is stored and default value not provided", () => {
    expect(AppStorage.getItem("foo")).toEqual("");
    expect(AppStorage.getItem("foo")).toEqual("");
    expect(AppStorage.getItem("foo")).toEqual("");
    expect(AppStorage.getItem("foo")).toEqual("");
    expect(AppStorage.getItem("foo")).toEqual("");
  });
  it("return 42 value or false when item is stored", () => {
    localStorage.setItem("foo", "bar");
    expect(AppStorage.getItem("foo", "toto")).toEqual("bar");
    expect(AppStorage.getItem("foo", false)).toEqual(false);
    expect(AppStorage.getItem("foo", true)).toEqual(false);
    expect(AppStorage.getItem("foo", 0)).toEqual(0);
    expect(AppStorage.getItem("foo", 13)).toEqual(13);
  });
  it("return boolean value when item is stored", () => {
    localStorage.setItem("foo", "true");
    expect(AppStorage.getItem("foo", "bar")).toEqual("true");
    expect(AppStorage.getItem("foo", false)).toEqual(true);
    expect(AppStorage.getItem("foo", true)).toEqual(true);
    expect(AppStorage.getItem("foo", 0)).toEqual(0);
    expect(AppStorage.getItem("foo", 13)).toEqual(13);
  });
  it("return boolean value when item is stored", () => {
    localStorage.setItem("foo", "42");
    expect(AppStorage.getItem("foo", "bar")).toEqual("42");
    expect(AppStorage.getItem("foo", false)).toEqual(false);
    expect(AppStorage.getItem("foo", true)).toEqual(false);
    expect(AppStorage.getItem("foo", 0)).toEqual(42);
    expect(AppStorage.getItem("foo", 13)).toEqual(42);
  });
});
